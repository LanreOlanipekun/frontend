import React, { Component } from 'react';
import '../assets/css/products.css';
import TopNav from './topNav'
import {
    Container,
    Card,
    Row,
    Col,
    Alert
} from 'reactstrap';
import { connect } from 'react-redux';
import {getProducts, clear} from '../actions/productActions';
import activating from '../assets/icons/activating.svg';
import { withRouter} from 'react-router';

class Products extends Component {
    _isMounted = false
    state = {
        loading:false,
        msg:null
    }
componentDidMount() {
    this._isMounted = true
    if (this._isMounted) {
        this.setState({
            loading: true
        })
    }
    this.props.clear()
    this.props.getProducts()
    .then(()=> {
        if (this._isMounted) {
            this.setState({
                loading:false
            })
        }
    })
    .catch(err => {
        if (this._isMounted) {
            this.setState({
                loading:false,
                msg:'Can not load product at the moment'
            })
        }
    })
}
componentWillUnmount() {
    this._isMounted = false
}
   
    render() {
        return (
            <div className="Products">
                
                <TopNav title={'Product list'} page={'product'} />

                {!this.state.loading ? <Container>
                    {this.state.msg ? <Alert color="danger" className="text-center">{this.state.msg}</Alert>:null}
                    <div className="list">
                        <Row>
                            <Col sm={{ size: 10, offset: 1 }}>
                                {this.props.products.products.length ? <Card>
                                    {this.props.products.products.map(({name, price, _id})=> (
                                        <Row className="bord" key={_id}>
                                        <Col sm={6}>
                                            <p className="name" >{name}</p>
                                        </Col>
                                        <Col sm={6}>
                                            <p className="text-right price">₦ {price.toLocaleString()}</p>
                                        </Col>
                                    </Row>
                                    ))}
                                    
                                   

                                </Card>:null}
                            </Col>
                        </Row>


                    </div>
                </Container>: <Row className="Products">
                        <Col sm={12} className="text-center">
                            <div className="activating">
                                <div className="loading">
                                    <img style={{ width: '70px' }} src={activating} alt="" />
                                </div>
                            </div>
                        </Col>
                    </Row>}
            </div>
        );
    }
}
const mapStateToProps = state => ({
    products: state.products

})
export default connect(mapStateToProps, {getProducts, clear})(withRouter(Products));