import axios from 'axios';

import {
    GET_PRODUCTS,
    ADD_PRODUCT,
    GET_A_PRODUCT,
    CLEAR_SINGLE
} from './types'


// get all products
export const getProducts = () => (dispatch) => {
    return new Promise((resolve, reject) => {
    axios.get('https://backend011.herokuapp.com/products/')
        .then(res => {
            dispatch({
                type: GET_PRODUCTS,
                payload: res.data.data
            })
            resolve()
        })
        .catch(err => {
            reject(err)
        })
    })
    
}
// get single product
export const getProduct = (id) => (dispatch) => {
    return new Promise((resolve, reject) => {
    axios.get('https://backend011.herokuapp.com/products/'+id)
        .then(res => {
            dispatch({
                type: GET_A_PRODUCT,
                payload: res.data.data
            })
            resolve()
        })
        .catch(err => {
            reject(err)
        })
    })
    
}

// add product
export const addProduct = (formData) => (dispatch) => {
    return new Promise((resolve, reject) => {
        axios.post('https://backend011.herokuapp.com/products', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
            .then(resp => {
                dispatch({
                    type: ADD_PRODUCT,
                    payload: resp.data.product
                })
                resolve(resp.data.product)
            })
            .catch(err => {
                reject(err)
            })
    })

}
export const clear = () => {
    return {
        type: CLEAR_SINGLE
    }
}



