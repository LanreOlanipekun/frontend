import React, {Component} from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import products from './components/products';
import preview from './components/preview';
import { Provider } from 'react-redux';
import store from './store';


class App extends Component {

  render(){
    return (
      <Provider store={store}>
      <div>
      <BrowserRouter>
        <div>
          <Route exact path="/" component={products} />
          <Route exact path="/preview/:id" component={preview} />
        </div>
      </BrowserRouter>
    </div>
    </Provider>
    );
  }
  
}

export default App;
